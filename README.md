### Sunspots API
##### Author: Christian R. Garcia
##### EID: CRG2965
#### Description:
This is an flask API that allows the user to get data, post data, create jobs, get jobs, and other variety of things using multiple workers which can be deployed on one VM or with many VMs. 

#### Requirements:
Docker - Needed in order to use Docker-Compose.  
Docker-Compose - Needed in order to deploy everything all at once.  
Curl/Requests - If you want to talk to the API that is.

#### Deploy and Use:
Documentation outlining all endpoints is in /docs, in the 'specifications.md' file.  
Documentation on deploying the API and workers is located in /docs, in the 'deployment.md' file.  
Images are all pushed to DockerHub and are located at: https://hub.docker.com/u/notchristiangarcia.

#### Credits:
Thank you to Nils Werner on Stack Exchange for creating an awesome atomic writer. https://goo.gl/1e8ByD  
Thank you everyone reading this as you're most likely my professors, you guys rock. 
